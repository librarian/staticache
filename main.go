package main

import (
	"fmt"
	"net/http"
	"time"

	"codeberg.org/librarian/staticache/api"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
)

func main() {
	viper.SetConfigName("config")
	viper.SetConfigType("yml")
	viper.AddConfigPath("/etc/librarian/staticache")
	viper.AddConfigPath("$HOME/.config/librarian/staticache")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()

	viper.SetDefault("PORT", "3001")
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Librarian started on port " + viper.GetString("PORT"))

	r := mux.NewRouter()
	r.HandleFunc("/find", api.FindVideo)
	r.HandleFunc("/cache", api.CacheVideoHandler)
	r.HandleFunc("/rss", api.RssHandler)

	srv := &http.Server{
		Handler:      r,
		Addr:         ":" + viper.GetString("PORT"),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	fmt.Println(srv.ListenAndServe())
}