package storage

import (
	"fmt"
	"os"

	"github.com/peterbourgon/diskv/v3"
)

func Database() *diskv.Diskv {
	flatTransform := func(s string) []string { return []string{} }

	homedir, err := os.UserHomeDir()
	if err != nil {
  	fmt.Println(err)
	}

	db := diskv.New(diskv.Options{
		BasePath:     homedir + "/.local/share/librarian/staticache",
		Transform:    flatTransform,
		CacheSizeMax: 1024 * 1024,
	})

	return db
}