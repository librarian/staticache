package storage

import (
	"context"
	"fmt"
	"os"
	"regexp"

	"github.com/spf13/viper"
	"github.com/web3-storage/go-w3s-client"
)

func UploadW3S(filePath string) string {
	client, err := w3s.NewClient(w3s.WithToken(viper.GetString("WEB3_STORAGE_TOKEN")))
	if err != nil {
		fmt.Println(err)
	}

	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println(err)
	}
	
	cid, err := client.Put(context.Background(), file)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Upload " + filePath + "complete. URL: " + viper.GetString("IPFS_GATEWAY") + cid.String())

	re := regexp.MustCompile(`(?m)(.*)/`)
	fileName := re.ReplaceAllString(filePath, "")

	os.Remove(filePath)
	return viper.GetString("IPFS_GATEWAY") + cid.String() + "/" + fileName
}