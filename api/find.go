package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"codeberg.org/librarian/staticache/storage"
	"github.com/spf13/viper"
	"github.com/tidwall/gjson"
)

func GetClaimId(url string) string {
	urls := []string{url}

	resolveDataMap := map[string]interface{}{
		"jsonrpc": "2.0",
		"method":  "resolve",
		"params": map[string]interface{}{
			"urls":                     urls,
			"include_purchase_receipt": true,
			"include_is_my_output":     true,
		},
		"id": time.Now().Unix(),
	}
	resolveData, _ := json.Marshal(resolveDataMap)
	videoDataRes, err := http.Post(viper.GetString("API_URL")+"?m=resolve", "application/json", bytes.NewBuffer(resolveData))
	if err != nil {
		fmt.Println(err)
	}
	videoDataBody, _ := ioutil.ReadAll(videoDataRes.Body)

	return gjson.Get(string(videoDataBody), "result.*.claim_id").String()
}

func GetDuration(url string) int64 {
	resolveDataMap := map[string]interface{}{
		"jsonrpc": "2.0",
		"method":  "resolve",
		"params": map[string]interface{}{
			"urls":                     []string{url},
			"include_purchase_receipt": true,
			"include_is_my_output":     true,
		},
		"id": time.Now().Unix(),
	}
	resolveData, _ := json.Marshal(resolveDataMap)
	videoDataRes, err := http.Post(viper.GetString("API_URL")+"?m=resolve", "application/json", bytes.NewBuffer(resolveData))
	if err != nil {
		fmt.Println(err)
	}
	videoDataBody, _ := ioutil.ReadAll(videoDataRes.Body)

	return gjson.Get(string(videoDataBody), "value.video.duration").Int()
}

func FindYtSync(id string) string {
	syncRes, err := http.Get("https://api.odysee.com/yt/resolve?video_ids="+id)
	if err != nil {
		fmt.Println(err)
	}

	syncBody, err2 := ioutil.ReadAll(syncRes.Body)
	if err2 != nil {
		fmt.Println(err2)
	}
	
	lbryUrl := gjson.Get(string(syncBody), "data.videos."+id).String()
	if lbryUrl != "" {
		return GetClaimId(lbryUrl)
	} else {
		return ""
	}
}

func FindVideo(w http.ResponseWriter, r *http.Request) {
	claimId := r.URL.Query().Get("claim_id")
	if claimId == "" {
		w.Write([]byte("missing claim_id"))
		return
	}

	db := storage.Database()

	fhdItm, err := db.Read(claimId + "-0")
	if err != nil {
		err = nil
	}
	hdItm, err := db.Read(claimId + "-1")
	if err != nil {
		err = nil
	}
	sdItm, err := db.Read(claimId + "-2")
	if err != nil {
		err = nil
	}

	data := make(map[string]string)
	data["1080p"] = string(fhdItm)
	data["720p"] = string(hdItm)
	data["480p"] = string(sdItm)

	json.NewEncoder(w).Encode(data)
}