package api

import (
	"net/http"
	"strings"

	"codeberg.org/librarian/staticache/storage"
	"github.com/mmcdole/gofeed"
	"github.com/spf13/viper"
)

func RssHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Query().Get("key") != viper.GetString("API_KEY") {
		w.Write([]byte("invalid api key"))
		return
	}

	lbryChannels := viper.GetStringSlice("LBRY_CHANNELS")
	ytChannels := viper.GetStringSlice("YT_CHANNELS")
	for i := 0; i < len(ytChannels); i++ {
		CompareFeeds(ytChannels[i], lbryChannels[i])
	}
}

func CompareFeeds(ytChannelId string, lbryChannel string) {
	db := storage.Database()
	lbryFeed, _ := gofeed.NewParser().ParseURL(viper.GetString("LIBRARIAN_URL") + lbryChannel + "/rss")

	latestItem := lbryFeed.Items[0]
	itemUrl := "lbry://" + strings.ReplaceAll(latestItem.Link, viper.GetString("LIBRARIAN_URL"), "")
	claimId := GetClaimId(itemUrl)
	_, err := db.Read(claimId + "-2")
	if err == nil {
		return
	} else {
		ytFeed, _ := gofeed.NewParser().ParseURL("https://www.youtube.com/feeds/videos.xml?channel_id=" + ytChannelId)

		if ytFeed.Items[0].Title == latestItem.Title {
			videoDuration := GetDuration(itemUrl)

			if !(videoDuration > viper.GetInt64("MAX_DURATION")) {
				CacheVideo(claimId, strings.ReplaceAll(ytFeed.Items[0].Link, "https://https://www.youtube.com/watch?v=", ""))
			}
		}
	}
}