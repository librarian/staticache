package api

import (
	"fmt"
	"net/http"
	"os/exec"

	"codeberg.org/librarian/staticache/storage"
	"github.com/spf13/viper"
)

func CacheVideoHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Query().Get("key") != viper.GetString("API_KEY") {
		w.Write([]byte("invalid api key"))
		return
	}

	id := r.URL.Query().Get("id")
	if id == "" {
		w.Write([]byte("missing YouTube video ID"))
		return
	}

	claimId := FindYtSync(id)
	if r.URL.Query().Get("claim_id") != "" {
		claimId = r.URL.Query().Get("claim_id")
	}
	if claimId == "" {
		w.Write([]byte("not synced to Odysee"))
		return
	}

	CacheVideo(claimId, id)
}

func CacheVideo(claimId string, id string) {
	formats := []string{
		"bv[width=1920][vcodec=vp9]+251",
		"bv[width=1280][vcodec=vp9]+251",
		"bv[width=854][vcodec=vp9]+251",
	}

	for i := 0; i < len(formats); i++ {
		filePath := viper.GetString("DL_DIR") + id + "-" + fmt.Sprint(i) + ".webm"

		cmd := exec.Command("yt-dlp", "-f", formats[i], "https://youtu.be/"+id, "-o", filePath)
		cmd.Run()

		url1 := storage.UploadW3S(filePath)
		db := storage.Database()

		db.Write(claimId + "-" + fmt.Sprint(i), []byte(url1))
	}
}