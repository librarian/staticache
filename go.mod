module codeberg.org/librarian/staticache

go 1.16

require (
	github.com/dgraph-io/badger/v3 v3.2103.2 // indirect
	github.com/dustin/go-humanize v1.0.0
	github.com/gomarkdown/markdown v0.0.0-20210514010506-3b9f47219fe7
	github.com/gorilla/feeds v1.1.1
	github.com/gorilla/mux v1.8.0
	github.com/kr/text v0.2.0 // indirect
	github.com/microcosm-cc/bluemonday v1.0.15
	github.com/mmcdole/gofeed v1.1.3 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/peterbourgon/diskv/v3 v3.0.1 // indirect
	github.com/spf13/viper v1.8.1
	github.com/tidwall/gjson v1.8.1
	github.com/web3-storage/go-w3s-client v0.0.4 // indirect
	go.etcd.io/bbolt v1.3.6 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
