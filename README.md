# staticache

Optimized video delivery for Odysee YouTube synced videos.

Staticache will download VP9 videos from YouTube and upload them to a storage provider, allowing for quality selection and reduced bandwidth with VP9 and reduce server load during video releases.